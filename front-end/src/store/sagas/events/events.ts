import { RootState } from '../../reducers/index';
import { FETCH_EVENTS_START } from '../../constants/constants';
import { select, put, takeEvery } from 'redux-saga/effects';
import { fetchEventSuccess, fetchEventFailed } from '../../actions/events/event.actions.ts';
import axios from 'axios';

export function* fetchEventStartAsync() {
  const recipientId = yield select((state: RootState) => state.event.recipientId);
  const searchDate = yield select((state: RootState) => state.event.searchDate);
  try {
      const res = yield axios({
          url: `http://localhost:8000/obs/${recipientId}/${searchDate}`,
          method: 'get',
          headers: { 'Content-Type': 'application/json' }
      });

      res.status === 200 && res.data.length >= 1 ? 
      yield put(fetchEventSuccess(res)) : 
      yield put(fetchEventFailed());
    } catch (e) {
      yield put(fetchEventFailed());
    }
}

export function* fetchEventStart() {
  yield takeEvery(FETCH_EVENTS_START, fetchEventStartAsync);
}

export default fetchEventStart;