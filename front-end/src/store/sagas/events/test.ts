import fetchEventStart, { fetchEventStartAsync } from './events';
import { put, takeEvery } from 'redux-saga/effects';
import { FETCH_EVENTS_START } from '@App/store/constants/constants';
import { fetchEventSuccess, fetchEventFailed } from '@App/store/actions/events/event.actions.ts';

describe('fetchEventStart saga', () => {
    it('should trigger on FETCH_EVENTS_START', () => {
      const generator = fetchEventStart();
      expect(generator.next().value).toEqual(
        takeEvery(FETCH_EVENTS_START, fetchEventStartAsync)
      );
    });
  });
  
describe('fetchEventStartAsync saga', () => {
    const mockRes = {
      data: [
        { payload_as_text: '{ "id": "1111", "event_type": "test1", "caregiver_id": "tt1", "timestamp": "tst1" }' }, 
        { payload_as_text: '{ "id": "11112", "event_type": "tesd", "caregiver_id": "tes", "timestamp": "te1" }' }, 
        { payload_as_text: '{ "id": "11113", "event_type": "te", "caregiver_id": "tt1", "timestamp": "test1" }' }
      ],
      status: 200,
      statusText: '',
      headers: '',
      config: {}
    };
    it('should fetchEvents succesfully', () => {
        const generator = fetchEventStartAsync();
        generator.next();
        generator.next();
        generator.next();
        expect(generator.next(mockRes).value).toEqual(
        put(fetchEventSuccess(mockRes)));
    });

    it('should be failed', () => {
        const newGenerator = fetchEventStartAsync();
        newGenerator.next();
        newGenerator.next();
        newGenerator.next();
        expect(newGenerator.next().value).toEqual(
        put(fetchEventFailed())
        );
    });
});