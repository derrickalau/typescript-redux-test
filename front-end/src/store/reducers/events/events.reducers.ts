import { FETCH_EVENTS_SUCCESS, FETCH_EVENTS_FAILED } from './../../constants/constants';
import { EventAction } from '../../actions/events/types';
import INITIAL_STATE from './initialState';
import { CHANGE_SEARCH_RECIPIENT_ID, CHANGE_SEARCH_DATE, FETCH_EVENTS_START } from '@App/store/constants/constants';

const eventReducer = (state = INITIAL_STATE, action: EventAction) => {
    switch (action.type) {
        case CHANGE_SEARCH_RECIPIENT_ID:
            return {
                ...state,
                recipientId: action.payload
            };

        case CHANGE_SEARCH_DATE:
            return {
                ...state,
                searchDate: action.payload
            };
            
        case FETCH_EVENTS_START:
            return {
                ...state,
                isFetching: true
            }; 

        case FETCH_EVENTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                events: action.payload
            };
        case FETCH_EVENTS_FAILED:
            return {
                ...state,
                isFetching: false,
                events: [{
                    id: 'Not Found',
                    timestamp: 'Not Found',
                    event_type: '',
                    caregiver_id: '',
                    description: []
                }]

            };
        default:
            return state;
    }
};

export default eventReducer;