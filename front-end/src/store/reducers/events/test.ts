import { Observation } from '@App/store/reducers/events/initialState';
import { 
    FETCH_EVENTS_START, 
    FETCH_EVENTS_SUCCESS, 
    FETCH_EVENTS_FAILED, 
    CHANGE_SEARCH_RECIPIENT_ID, 
    CHANGE_SEARCH_DATE } from '../../constants/constants';
import eventReducer from './events.reducers';
import { EventState } from './initialState';

const INITIAL_STATE: EventState = {
    recipientId: '',
    searchDate: new Date(),
    events: [{
        id: '',
        timestamp: '',
        event_type: '',
        caregiver_id: '',
        description: []
    }],
    isFetching: false
};

describe('eventReducer', () => {

    it('should SET recipientId to payload if CHANGE_SEARCH_RECIPIENT_ID', () => {
        const mockRecipientId = '1';
        expect(
            eventReducer(INITIAL_STATE, {
            type: CHANGE_SEARCH_RECIPIENT_ID,
            payload: mockRecipientId
        })
        ).toEqual({
          ...INITIAL_STATE,
          recipientId: mockRecipientId
        });
    });

    it('should SET searchDate to payload if CHANGE_SEARCH_DATE', () => {
        const mockSearchDate = new Date();
        expect(
            eventReducer(INITIAL_STATE, {
            type: CHANGE_SEARCH_DATE,
            payload: mockSearchDate
        })
        ).toEqual({
          ...INITIAL_STATE,
          searchDate: mockSearchDate
        });
    });

    it('should set isFetching to true if FETCH_EVENTS_START action', () => {
        expect(
            eventReducer(INITIAL_STATE, {
            type: FETCH_EVENTS_START
        }).isFetching
        ).toBe(true);
    });

    it('should set isFetching to false and Events to payload if FETCH_EVENTS_SUCCESS', () => {
        const mockEvents: Observation[] = [{
            id: '',
            timestamp: '',
            event_type: '',
            caregiver_id: '',
            description: []
        }];
        expect(
            eventReducer(INITIAL_STATE, {
            type: FETCH_EVENTS_SUCCESS,
            payload: mockEvents
        })
        ).toEqual({
        ...INITIAL_STATE,
        isFetching: false,
        events: mockEvents
        });
    });

    it('should set isFetching to false if FETCH_EVENTS_FAILED', () => {
        const mockEvents: Observation[] = [{
            id: 'Not Found',
            timestamp: 'Not Found',
            event_type: '',
            caregiver_id: '',
            description: []
        }];
        expect(
            eventReducer(INITIAL_STATE, {
            type: FETCH_EVENTS_FAILED
        })
        ).toEqual({
        ...INITIAL_STATE,
        events: mockEvents,
        isFetching: false
        });
    });
});