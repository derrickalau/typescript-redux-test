
export interface Observation {
    id: string;
    timestamp: string;
    event_type: string;
    caregiver_id: string;
    description: JSX.Element[];
}

export interface EventState {
    events: Observation[];
    recipientId: string;
    searchDate: Date;
    isFetching: boolean;

}

const INITIAL_STATE: EventState = {
    recipientId: '',
    searchDate: new Date(),
    events: [{
        id: '',
        timestamp: '',
        event_type: '',
        caregiver_id: '',
        description: []
    }],
    isFetching: false
};

export default INITIAL_STATE;