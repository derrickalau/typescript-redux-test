
import { combineReducers } from '@reduxjs/redux-toolkit';

import eventReducer from './events/events.reducers';

export const rootReducer = combineReducers({
    event: eventReducer
});

export type RootState = ReturnType<typeof rootReducer>;
