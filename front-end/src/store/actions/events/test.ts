import { fetchEventStart, setSearchRecipientId, setSearchDate } from './event.actions.ts';
import { 
    FETCH_EVENTS_START, 
    CHANGE_SEARCH_RECIPIENT_ID, CHANGE_SEARCH_DATE } from '@App/store/constants/constants';

describe('setSearchRecipientId action', () => {
    const mockvalue = '1';
    it('should create the fetchEventStart action', () => {
    expect(setSearchRecipientId(mockvalue).type).toEqual(
        CHANGE_SEARCH_RECIPIENT_ID
    );
    });
});

describe('setSearchDate action', () => {
    const mockvalue = '2020-9-9';
    it('should create the setSearchDate action', () => {
    expect(setSearchDate(mockvalue).type).toEqual(
        CHANGE_SEARCH_DATE
    );
    });
});

describe('fetchEventStart action', () => {
    it('should create the fetchEventStart action', () => {
    expect(fetchEventStart().type).toEqual(
        FETCH_EVENTS_START
    );
    });
});