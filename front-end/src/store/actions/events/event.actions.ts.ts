import { CHANGE_SEARCH_RECIPIENT_ID, 
    CHANGE_SEARCH_DATE, FETCH_EVENTS_START,
    FETCH_EVENTS_SUCCESS, FETCH_EVENTS_FAILED } from '@App/store/constants/constants';
import { AxiosResponse } from 'axios';
import EventListMaker from '@App/components/tools/EventListMaker';

export const setSearchRecipientId = (value:  string) => ({
    type: CHANGE_SEARCH_RECIPIENT_ID,
    payload: value
});

export const setSearchDate = (value: string) => ({
    type: CHANGE_SEARCH_DATE,
    payload: value
});

export const fetchEventStart = () => ({
    type: FETCH_EVENTS_START
});

export const fetchEventSuccess = (res: AxiosResponse) => ({
    type: FETCH_EVENTS_SUCCESS,
    payload: EventListMaker(res.data)
});

export const fetchEventFailed = () => ({
    type: FETCH_EVENTS_FAILED
});