import { CHANGE_SEARCH_RECIPIENT_ID, 
    CHANGE_SEARCH_DATE, FETCH_EVENTS_START, 
    FETCH_EVENTS_FAILED, FETCH_EVENTS_SUCCESS } from './../../constants/constants';
import { SET_EVENT_NOT_FOUND, SET_EVENTS } from '../../constants/constants';
import { Observation } from '@App/store/reducers/events/initialState';

export interface ActionChangeSearchRecipientId {
    type: typeof CHANGE_SEARCH_RECIPIENT_ID;
    payload: string;
}

export interface ActionChangeSearchDate {
    type: typeof CHANGE_SEARCH_DATE;
    payload: Date;
}

export interface ActionSetEvents {
    type: typeof SET_EVENTS;
    payload: Observation[];
}

export interface ActionSetNotFound {
    type: typeof SET_EVENT_NOT_FOUND;
}

export interface ActionFetchEventStart {
    type: typeof FETCH_EVENTS_START;
}

export interface ActionFetchEventSuccess {
    type: typeof FETCH_EVENTS_SUCCESS;
    payload: Observation[];
}

export interface ActionFetchEventFailed {
    type: typeof FETCH_EVENTS_FAILED;
}

export type EventAction = ActionChangeSearchRecipientId | 
ActionChangeSearchDate | ActionSetNotFound | ActionFetchEventStart |
ActionSetEvents | ActionFetchEventFailed | ActionFetchEventSuccess;
