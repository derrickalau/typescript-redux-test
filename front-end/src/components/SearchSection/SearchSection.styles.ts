import styled from 'styled-components';

const IBStyle = `
  background: transparent;
  border-radius: 3px;
  border: 2px solid #54c6c1;
  color: black;
  margin: 0.5em 1em;
  padding: 0.25em 1em;
`;

export const Button = styled.button`${IBStyle}`;

export const Input = styled.input`${IBStyle}`;

export const Table = styled.table`  
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
`;

export const Th = styled.th`  
  text-align: left;
  padding: 8px;
  background: #54c6c1; 
	color: white; 
  font-weight: bold; 
  padding: 10px; 
	border: 1px solid #ccc; 
	text-align: left; 
	font-size: 3.2vmin;
`;

export const Td = styled.td`  
  padding: 10px; 
  border: 1px solid #ccc; 
  text-align: left; 
	font-size: 3vmin;
`;

export const Section = styled.section`  
    width:100%;
    max-height:100%;
    margin-top: 4vh;
    overflow: auto;
`;