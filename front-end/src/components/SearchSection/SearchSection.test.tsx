import * as React    from 'react';
import { shallow, ShallowWrapper }   from 'enzyme';
import SearchSection from '@App/components/SearchSection';

describe('SearchSection component', () => {
    let wrapper: ShallowWrapper<Readonly<{}>, React.Component<{}>>;
    let mockApiReq: jest.Mock = jest.fn();
    let mockHandleRecipientId: jest.Mock = jest.fn();
    let mockHandleDate: jest.Mock = jest.fn();
    let mockMappedEvents = [(
        <tr key={1}>
            <th scope="row">test1</th>
            <td>test2</td>
            <td>test3</td>
            <td>test4</td>
        </tr>
    )];

    wrapper = shallow (
    <SearchSection
        InputName1="recipientID"
        InputType1="text"
        InputPh1="recipientID"
        InputName2="Date"
        InputType2="date"
        InputPh2="Date"
        SearchInput={mockHandleRecipientId}
        SearchInput2={mockHandleDate}
        SearchRequest={mockApiReq}
        mappedTable={mockMappedEvents}
        theadID="Date"
        thead1="Event Type"
        thead2="Caregiver ID"
        thead3="Event Description"
    />);

    it('should render SearchSection component', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('should call mockApiReq when search button is clicked', () => {
        wrapper.find('[id=\'searchButton\']').simulate('click');
        expect(mockApiReq).toHaveBeenCalled();
    });

    it('should call mockHandleRecipientId when input1 is changed', () => {
        wrapper.find('[id=\'input1\']').simulate('change');
        expect(mockHandleRecipientId).toHaveBeenCalled();
    });

    it('should call mockHandleDate when input2 is changed', () => {
        wrapper.find('[id=\'input2\']').simulate('change');
        expect(mockHandleDate).toHaveBeenCalled();
    });

});