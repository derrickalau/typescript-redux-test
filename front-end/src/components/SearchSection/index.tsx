import * as React from 'react';
import { Section, Input, Button, Table, Th } from './SearchSection.styles';

interface Ssection {
  InputName1: string;
  InputType1: string;
  InputPh1: string;
  InputName2: string;
  InputType2: string;
  InputPh2: string;
  SearchInput: (event: React.ChangeEvent<HTMLInputElement>) => {};
  SearchInput2: (event: React.ChangeEvent<HTMLInputElement>) => {};
  SearchRequest: () => void;
  mappedTable: JSX.Element[];
  theadID: string;
  thead1: string;
  thead2: string;
  thead3: string;
                                             
}

const SearchSection: React.FC<Ssection> = ({
  InputName1, InputType1, InputPh1,
  InputName2, InputType2, InputPh2,
  SearchInput, SearchRequest, SearchInput2, mappedTable, 
  theadID, thead1, thead2, thead3
}) => {                                    
  return (
    <>
    <Section>
      <Input id={'input1'} name={InputName1} type={InputType1} placeholder={InputPh1} onChange={SearchInput}/>
      <Input id={'input2'} name={InputName2} type={InputType2} placeholder={InputPh2} onChange={SearchInput2}/>
      <Button id={'searchButton'} onClick={SearchRequest}>
        Search
      </Button>
      <Table>
        <thead>
          <tr><Th>{theadID}</Th><Th>{thead1}</Th><Th>{thead2}</Th><Th>{thead3}</Th></tr>
        </thead>
        <tbody>
          {mappedTable}
        </tbody>
      </Table>
    </Section>
    </>
  );
};
  
export default SearchSection;
