import { Observation } from '@App/store/reducers/events/initialState';
import * as React from 'react';

interface Payload {
    payload_as_text: string;
}

export const MergeProperties = (text: Object): JSX.Element[] => {
    
    const res = Object.keys(text).reduce((acc: Object, elem: string) => {
        if (text[elem].length > 0 && elem !== 'timestamp' && elem !== 'routes' 
            && elem !== 'event_type' && elem !== 'caregiver_id' && elem !== 'observations'
            && elem !== 'caregiver_id' && elem !== 'visit_id' && elem !== 'task_definition_id'
            && elem !== 'task_schedule_id' && elem !== 'id' && elem !== 'care_recipient_id'
            && elem !== 'task_instance_id' && elem !== 'navigation') { 
                acc[elem] = text[elem]; 
        }
        return acc;
    },                                   {});

    return JSON.stringify(res)
        .replace(/['"{}]+/g, '   ')
        .split(' /n/n/n ')
        .map( (item: string, i: number) => <p key={i}>{item}<br/></p> );
};

const EventListMaker = ( payloadTexts: Object[] ): Observation[] => (

    payloadTexts.map((payload: Payload) => {
        let text: Observation =  JSON.parse(payload.payload_as_text);
        let observation = {
                id: text.id,
                timestamp: new Date(text.timestamp).toLocaleTimeString(),
                event_type: text.event_type,
                caregiver_id: text.caregiver_id,
                description: MergeProperties(text)
            };
        return observation;
    })
);

export default EventListMaker;
