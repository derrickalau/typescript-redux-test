import EventListMaker, { MergeProperties } from './EventListMaker';

describe('MergeProperties', () => {
    let mocktext1: Object =  {
        id: '42cc91d2-35e8-42b0-804b-85ffca12a036',
        timestamp: '2019-05-12T07:20:47.169Z',
        event_type: 'task_completed',
        caregiver_id: '3cacba0a-6041-11e9-b63f-06a80bfbb33e',
        task_schedule_id: 'eb421e26-cee6-4f10-a79e-7d248a87e3b3',
        task_schedule_note: '',
        task_definition_description: 'Ensure required items are close to hand'
    };

    it('should return an array.lenth >= 1', () => {
        expect(MergeProperties(mocktext1).length).toEqual(1);
    });

});

describe('EventListMaker', () => {

    const mockPayload = [
        { payload_as_text: '{ "id": "1111", "event_type": "test1", "caregiver_id": "test1", "timestamp": "test1" }' }, 
        { payload_as_text: '{ "id": "1111", "event_type": "test1", "caregiver_id": "test1", "timestamp": "test1" }' }, 
        { payload_as_text: '{ "id": "1111", "event_type": "test1", "caregiver_id": "test1", "timestamp": "test1" }' }
    ];

    const mockPayload2 = [{payload_as_text: '{}'}];

    it('should return array.length = 3', () => {
        expect(EventListMaker(mockPayload).length).toEqual(3);
    });

    it('should return array.length = 1', () => {
        expect(EventListMaker(mockPayload2).length).toEqual(1);
    });
});