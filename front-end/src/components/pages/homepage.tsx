import * as React    from 'react';
import SearchSection from '@App/components/SearchSection';
import { Dispatch, AnyAction }    from 'redux';
import { Td } from '../SearchSection/SearchSection.styles';
import { setSearchRecipientId, setSearchDate, fetchEventStart } from '@App/store/actions/events/event.actions.ts';
import { RootState } from '@App/store/reducers';
import { connect } from 'react-redux';
import { Observation } from '@App/store/reducers/events/initialState';

interface Props {
    events: Observation[];
    SetSearchRecipientId: (event:  React.ChangeEvent<HTMLInputElement>) => {};
    SetSearchDate: (event:  React.ChangeEvent<HTMLInputElement>) => {};
    FetchEventStart: () => {type: string; };
}

export const Homepage: React.FC<Props> = ({ events, SetSearchRecipientId, SetSearchDate, FetchEventStart}) =>  (
    <SearchSection
        InputName1="recipientID"
        InputType1="text"
        InputPh1="recipientID"
        InputName2="Date"
        InputType2="date"
        InputPh2="Date"
        SearchInput={SetSearchRecipientId}
        SearchInput2={SetSearchDate}
        SearchRequest={FetchEventStart}
        mappedTable={events.map(
            ({id, timestamp, event_type, caregiver_id, description}) => (
                <tr key={id}>
                    <th scope="row">{timestamp}</th>
                    <Td>{event_type}</Td>
                    <Td>{caregiver_id}</Td>
                    <Td>{description}</Td>
                </tr>
            )
        )}
        theadID="Time"
        thead1="Event Type"
        thead2="Caregiver ID"
        thead3="Event Description"
    />
);

const mapStateToProps = (state: RootState) => ({
    events: state.event.events
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) => ({
    SetSearchRecipientId: (event: React.ChangeEvent<AnyAction>) => dispatch(setSearchRecipientId(event.target.value)),
    SetSearchDate: (event: React.ChangeEvent<AnyAction>) => dispatch(setSearchDate(event.target.value)),
    FetchEventStart: () => dispatch(fetchEventStart())
});

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);