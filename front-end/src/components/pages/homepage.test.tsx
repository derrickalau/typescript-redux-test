import * as React    from 'react';
import { shallow }   from 'enzyme';
import { Homepage } from './homepage';

describe('SearchSection component', () => {
    it('should render homepage component', () => {
        expect(shallow (
        <Homepage 
            events={[]} 
            SetSearchRecipientId={jest.fn} 
            SetSearchDate={jest.fn} 
            FetchEventStart={jest.fn()} 
        />)).toMatchSnapshot();
    });
});