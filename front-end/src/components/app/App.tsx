import * as React                    from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import Logo                          from '../../assets/Logo';
import Homepage                      from '@App/components/pages/homepage';
const LogoUrl = require('../../assets/images/logo-birdie.svg');

const GlobalStyle = createGlobalStyle`
  body {
    padding:1em;
    background-color: #F9F9F9;
    > div {
      height: 100%;
    }
  }
`;

const AppContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

class App extends React.Component {
  
    public render() {
        return (
            <>
                <GlobalStyle />
                <AppContainer>
                    <Logo src={LogoUrl}/>
                    <Homepage />
                </AppContainer>
            </>
        );
    }
}

export default App;