// tslint:disable-next-line:no-implicit-dependencies
import * as request from 'supertest';
import {app} from '../src/application'

describe('GET /obs/:care_recipient_id', () => {
  it('should find a list of events BY care_recipient_id and Date', async () => {
    await request(app)
      .get('/obs/df50cac5-293c-490d-a06c-ee26796f850d/2019-05-12')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect( ( res: request.Response ) => {
        expect(res.body.length).toBeGreaterThan(1)
      })
  });
    it('should respond with 404 Not Found', async() => {
        await request(app)
            .get('/obs/idisnonexisting/timeisnonexisting')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404)
            .expect( ( res: request.Response ) => {
                expect( res.body ).toBeNull();})
    });
});

