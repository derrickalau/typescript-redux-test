import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as helmet from 'helmet';
import * as mysql from 'mysql';

import {getObservationsController} from "./controllers/getObservations";

dotenv.config();

export const app = express();


export const connection: mysql.Connection = mysql.createConnection({
    database : 'birdietest',
    host     : 'birdie-test.cyosireearno.eu-west-2.rds.amazonaws.com',
    password : 'xnxPp6QfZbCYkY8',
    user     : 'test-read',
});

app.use(cors());
app.use(helmet());
app.use(getObservationsController);

