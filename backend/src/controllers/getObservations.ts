
import * as express from "express";
import { connection } from './../application';
export const getObservationsController = express.Router();


getObservationsController.get('/obs/:care_recipient_id/:timestamp', (req:express.Request, res:express.Response) => {
  const q: string = `
      SELECT payload_as_text FROM events 
      WHERE care_recipient_id = '${req.params.care_recipient_id}' 
        AND timestamp LIKE '%${req.params.timestamp}%' 
      ORDER BY timestamp DESC
    `;
  connection.query(q, (error, results) => {
    if(error) {res.status(400).json(error);}
    else if(results.length < 1) {res.status(404).json(error);}
    else {res.status(200).json(results);}
  })
  
})